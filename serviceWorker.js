const assets = [
  "/",
  "/index.html",
  "/index.css",
  "/index.js",
  "https://yt3.ggpht.com/a/AGF-l79ZFESTFzhfpr7QibKjQ-AnjZnfISdxCPSiHg=s900-c-k-c0xffffffff-no-rj-mo"
]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open("patwary-search").then(cache => {
      cache.addAll(assets)
    })
  )
})
self.addEventListener("fetch", fetchEvent => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then(res => {
      return res || fetch(fetchEvent.request)
    })
  )
})